//Carousel

$('.portfolio-gallery-slides').anoSlide(
{
  items: 1,
  speed: 500,
  prev: 'button.portfolio-gallery-arrow-left',
  next: 'button.portfolio-gallery-arrow-right',
  lazy: true,
  // auto: 4000
});

//Scrollbar
// FF on OSX is not supported
// https://github.com/Diokuz/baron/issues/110
if (!isFFMac()) {
  $('.scroller').baron({
    direction: 'h',
    bar: '.scroller-bar',
    track: '.scroller-track',
    scrollingCls: '_scrolling'
  });
}

function isFFMac() {
  return !(window.mozInnerScreenX == null) && navigator.platform.indexOf('Mac')>=0;
};
